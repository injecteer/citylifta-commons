package com.crowdtaxi.commons.math;

public class Pricer {
  
  //TODO: adjust seat calc
  public static int getPrice( float pricePerKm, double distanceInM, int seats ){
    int basePrice = (int)( distanceInM * pricePerKm );
    if( 1 < seats ) basePrice = (int)( 1f * basePrice * ( 1 + 1f * seats / 10 ) ); 
    return basePrice;
  }

}
