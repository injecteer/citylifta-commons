package com.crowdtaxi.commons.math;

public class Utils {
  
  static final double R = 6378.137; // Earth radius in km
  
  /**
   * 
   * @param lat1
   * @param lon1
   * @param lat2
   * @param lon2
   * @return array with 0 -> distance in km
   *                    1 -> initial bearing in grad
   *                    2 -> final bearing in grad
   */
  public static double[] distanceAndBearings( double lat1, double lon1, double lat2, double lon2 ) {
    // Convert lat/long to radians
    double lat1R = Math.toRadians( lat1 );
    double lat2R = Math.toRadians( lat2 );
    double lon1R = Math.toRadians( lon1 );
    double lon2R = Math.toRadians( lon2 );
    
    double sinLat1R = Math.sin( lat1R );
    double sinLat2R = Math.sin( lat2R );
    double cosLat1R = Math.cos( lat1R );
    double cosLat2R = Math.cos( lat2R );
    
    double distance = Math.acos( sinLat1R * sinLat2R + cosLat1R * cosLat2R * Math.cos( lon2R - lon1R ) ) * R;
    
    double dLon = Math.toRadians( lon2 - lon1 );
    double y = Math.sin( dLon ) * cosLat2R;
    double x = cosLat1R * sinLat2R - sinLat1R * cosLat2R * Math.cos( dLon );
    double bearingRaw = Math.toDegrees( Math.atan2( y, x ) );
    
    return new double[]{ distance, ( bearingRaw + 360 ) % 360, ( bearingRaw + 180 ) % 360 };
  }
  
  /**
   * 
   * @param lat1
   * @param lon1
   * @param lat2
   * @param lon2
   * @return distance in km
   */
  public static double distance( double lat1, double lon1, double lat2, double lon2 ) {
    // Convert lat/long to radians
    double lat1R = Math.toRadians( lat1 );
    double lat2R = Math.toRadians( lat2 );
    double lon1R = Math.toRadians( lon1 );
    double lon2R = Math.toRadians( lon2 );
    return Math.acos( Math.sin( lat1R ) * Math.sin( lat2R ) + Math.cos( lat1R ) * Math.cos( lat2R ) * Math.cos( lon2R - lon1R ) ) * R;
  }
  
  /**
   * 
   * @param lat1
   * @param lon1
   * @param lat2
   * @param lon2
   * @return bearing in grad
   */
  public static double bearing( float lat1, float lon1, float lat2, float lon2 ) {
    double brng = bearingRaw( lat1, lon1, lat2, lon2 );
    return ( Math.toDegrees( brng ) + 360 ) % 360;
  }
  
  public static double finalBearing( float lat1, float lon1, float lat2, float lon2 ) {
    double brng = bearingRaw( lat1, lon2, lat2, lon1 );
    return ( Math.toDegrees( brng ) + 180 ) % 360;
  }

  public static double bearingRaw( float lat1, float lon1, float lat2, float lon2 ) {
    // Convert lat/long to radians
    double lat1R = Math.toRadians( lat1 );
    double lat2R = Math.toRadians( lat2 );
    double dLon = Math.toRadians( lon2 - lon1 );
    double y = Math.sin( dLon ) * Math.cos( lat2R );
    double x = Math.cos( lat1R ) * Math.sin( lat2R ) - Math.sin( lat1R ) * Math.cos( lat2R ) * Math.cos( dLon );
    return Math.atan2( y, x );
  }
  
  public static float[] pointAtBearing( float lat1, float lon1, double bearing, float dist ) {
    dist = (float)( dist / R );  // convert dist to angular distance in radians
    bearing = (float)Math.toRadians( bearing ); 
    double lat1R = Math.toRadians( lat1 );
    double lon1R = Math.toRadians( lon1 );

    double lat2 = Math.asin( Math.sin( lat1R ) * Math.cos( dist ) + Math.cos( lat1R ) * Math.sin( dist ) * Math.cos( bearing ) );
    double lon2 = lon1R + Math.atan2( Math.sin( bearing ) * Math.sin( dist ) * Math.cos( lat1R ), Math.cos( dist ) - Math.sin( lat1R ) * Math.sin( lat2 ) );
    lon2 = (lon2 + 3 * Math.PI ) % ( 2 * Math.PI ) - Math.PI;  // normalise to -180..+180º

    return new float[]{ (float)Math.toDegrees( lat2 ), (float)Math.toDegrees( lon2 ) };
  }
  
  public static double[] distancesOrthodromeToPoint( double startLat, double startLon, double lat, double lon, double bearing ) {
    double[] distAndBearigns2p = distanceAndBearings( startLat, startLon, lat, lon );
    double dist2p = distAndBearigns2p[ 0 ] / R;
    double crossTrackDist = Math.asin( Math.sin( dist2p ) * Math.sin( Math.toRadians( distAndBearigns2p[ 1 ] ) - Math.toRadians( bearing ) ) );
    double alongTrackDist = Math.acos( Math.cos( dist2p ) / Math.cos( crossTrackDist ) ) * R;
    return new double[]{ crossTrackDist * R, alongTrackDist };
  }
  
  public static float[] midPoint( float lat1, float lon1, float lat2, float lon2 ) {
    double lat1R = Math.toRadians( lat1 );
    double lon1R = Math.toRadians( lon1 );
    double lat2R = Math.toRadians( lat2 );
    double dLon = Math.toRadians( lon2 - lon1 );

    double Bx = Math.cos( lat2R ) * Math.cos( dLon );
    double By = Math.cos( lat2R ) * Math.sin( dLon );

    double lat3 = Math.atan2( Math.sin( lat1R ) + Math.sin( lat2R ), Math.sqrt( ( Math.cos( lat1R ) + Bx ) * ( Math.cos( lat1R ) + Bx ) + By * By ) );
    double lon3 = lon1R + Math.atan2( By, Math.cos( lat1R ) + Bx );
    lon3 = ( lon3 + 3 * Math.PI ) % ( 2 * Math.PI ) - Math.PI;  // normalise to -180..+180º
    
    return new float[]{ (float)Math.toDegrees( lat3 ), (float)Math.toDegrees( lon3 ) };
  }
  
}
